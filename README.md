# Communicating Emotion with Text

## What is this?

Just a bunch of html, javascript and css serving as a proof of concept of a UX of a feature idea that modern text messaging apps can benefit from (imho). 

More on that in my Youtube video here: https://www.youtube.com/watch?v=-hHQIOF1-GE

The script is just a demo of a way how you can communicate the tone of voice and emotion by swiftly altering font weight with a touch. 
Inspired by the following study here: https://arxiv.org/pdf/2202.10631.pdf

## Licence

CC0 

https://creativecommons.org/share-your-work/public-domain/cc0/
