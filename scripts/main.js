let message = "Yes, godfather's penis"
let messageArray = message.split("");

let touchStartedAt = {
    x: 0, 
    y: 0
}

const movementAreaLimit = 50; 
// const touchOffset = 100;
const touchOffset = 100;

function scale(number, inMin, inMax, outMin, outMax) {
    return (number - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
}

document.addEventListener("DOMContentLoaded", function (event) {
    
    for (let index = 0; index < messageArray.length; index++) {
        var characterContainer = document.createElement('div');
        document.querySelector("#wrapper").appendChild(characterContainer);
        characterContainer.classList.add("characterContainer");
        let char = messageArray[index]; 
        if (char == " ") {
            characterContainer.classList.add("space");
        }
        characterContainer.innerHTML = char;
    }

    let pathElements = document.getElementsByClassName("characterContainer");

    document.addEventListener("touchstart", function (event) {
        touchStartedAt.x = event.touches[0].clientX;
        touchStartedAt.y = event.touches[0].clientY;
    });

    document.addEventListener("touchmove", function (event) {
        
        document.querySelector("#test").innerHTML = event.touches[0].clientX + " | " + event.touches[0].clientY;

        let x = event.touches[0].clientX;
        let y = event.touches[0].clientY;

        let movementBoundaryTop = touchStartedAt.y + movementAreaLimit;
        let movementBoundaryBottom = touchStartedAt.y - movementAreaLimit;

        let fontWeight = Math.ceil(scale(y, movementBoundaryTop, movementBoundaryBottom, 100, 900));

        document.querySelector("#pointer").style.height = touchOffset + "px";
        document.querySelector("#pointer").style.top = y - touchOffset + "px"; 
        document.querySelector("#pointer").style.left = x + "px"; 
        
        for (let index = 0; index < pathElements.length; index++) {

            var rect = pathElements[index].getBoundingClientRect();
            console.log(rect);
            
            if(x >= rect.left && x <= rect.right) {

                if(touchStartedAt.y - touchOffset >= rect.top && touchStartedAt.y - touchOffset <= rect.bottom) {
                    pathElements[index].style.fontWeight = fontWeight;
                }
            }
        }
    });
    
    let alterPath = function(event) {
        var rect = event.target.getBoundingClientRect();
        var x = event.clientX - rect.left; //x position within the element.
        var y = event.touches[0].clientY - rect.top;
        let fontWeight = Math.ceil(scale(y, 0, 140, 900, 100));
        this.style.fontWeight = fontWeight;
    };

});

